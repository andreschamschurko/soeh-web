export class StreamServer
{
    name : string;
    url : string;
    imageUrl : string;
    streamTitle : string;


    constructor(server : {name : string, url : string, imageUrl : string, streamTitle : string})
    {
        this.name = server.name;
        this.url = server.url;
        this.imageUrl = server.imageUrl;
        this.streamTitle = server.streamTitle;
    }
}