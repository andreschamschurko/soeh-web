export class TreeNodeDLNAContainer
{
    // $
    id : string;
    parentID : string;
    restricted : number;
    searchable : number;

    // dc
    creator = "";
    publisher = "";
    title = "";

    // upnp
    album = "";
    albumArtURI = "";
    albumArtist = "";
    performer = "";
    class = "";
    episodeSeason = "";
    genre = new Array();

    // xbmc
    artworks = new Array();
    rating = "";
    userRating = "";

    items ?: TreeNodeDLNAContainer[];
    isDirectory : boolean;
    raw : any;

    constructor(container : any, isDummy : boolean)
    {
        this.id = container.$.id;
        this.parentID = container.$.parentID
        this.restricted = container.$.restricted;
        this.searchable = container.$.searchable;

        if (!isDummy)
        {
            this.creator = this.concatenateFromArray(container["dc:creator"]);
            this.publisher = this.concatenateFromArray(container["dc:publisher"]);
            this.title = this.concatenateFromArray(container["dc:title"]);

            this.album = container["upnp:album"]
            if (container["upnp:albumArtURI"] != undefined)
                this.albumArtURI = container["upnp:albumArtURI"][0]["_"];

            if (container["upnp:artist"] != undefined)
            {
                for (let i = 0; i < container["upnp:artist"].length; i++)
                {
                    let object = container["upnp:artist"][i];
                    
                    if (object.$ != undefined)
                    {
                        switch (object.$.role)
                        {
                            case "Performer":
                                if (this.performer == "")
                                    this.performer = object._;
                                else
                                    this.performer += " & " + object._;
                                break;
                            case "AlbumArtist":
                                if (this.albumArtist == "")
                                    this.albumArtist = object._;
                                else
                                    this.albumArtist += " & " + object._;
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            this.class = container["upnp:class"][0];
            this.episodeSeason = container["upnp:episodeSeason"][0];
            this.genre = container["upnp:genre"];

            for (let i = 0; i < container["xbmc:artwork"].length; i++)
            {
                this.artworks.push(container["xbmc:artwork"][i]._);
            }
            this.rating = container["xmbc:rating"];
            this.userRating = container["xmbc:userrating"];
        }

        this.items = [];

        this.isDirectory = true;
        this.raw = container;
    }

    private concatenateFromArray(array : Array<string>)
    {
        let stringToBuild = "";
        if (array != undefined)
        {
            stringToBuild = array[0]
            for (let i = 1; i < array.length; i++)
            {
                stringToBuild += " & " + array[i];
            }
        }

        return stringToBuild;
    }

    findNodeById(id : string)
    {
        if (this.id == id)
            return this;
        else
        {
            let foundNode = null;
            this.items?.forEach(function(item) {
                let foundNodeInItem = item.findNodeById(id)
                if (foundNodeInItem != null)
                {
                    foundNode = foundNodeInItem
                }
            })

            return foundNode;
        }
    }
}

export class TreeNodeDLNAItem extends TreeNodeDLNAContainer
{

    res : string;
    contentType : string;

    constructor(item : any)
    {
        super(item, false);
        this.res = item.$.res;
        this.contentType = item.$.contentType;

        this.isDirectory = false;
    }
}