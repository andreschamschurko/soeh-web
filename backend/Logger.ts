let config = require('config');
let winston = require('winston');
let { combine, timestamp, errors } = winston.format;
require('winston-daily-rotate-file');

let fileRotateTransport = new winston.transports.DailyRotateFile({
    filename: 'log/soeh-%DATE%.log',
    datePattern: 'YYYY-MM-DD',
    maxFiles: '14d',
});

function padLevel(level: String)
{
    let padding = ' '.repeat('verbose'.length - level.length);
    let ret = level + padding;
    return ret;
}

let logger = winston.createLogger({
    level: config.get("app.logger-level"),
    format: combine(
		winston.format.timestamp({ format: 'YYYY/MM/DD HH:mm:ss' }),
        winston.format.printf(
        (info: any) => `${info.timestamp}|${padLevel(info.level)}| ${info.message}`
        )
    ),
    transports: [
        fileRotateTransport
    ],
});
  
module.exports = logger;

