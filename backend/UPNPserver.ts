import fetch from 'node-fetch';

export class UPNPServer
{
    // header
    location : string;
    cacheControl : string;
    server : string;
    ext : string;
    upnpBootID : number;
    usn : string;
    st : string;
    date : string;

    // info
    ip : string;
    family : string;
    port : number;
    size : number;

    controlUrl = "";
    friendlyName = "";
    sid ?: number;

    static async createNewUPNPServer(header : {LOCATION : string, "CACHE-CONTROL" : string, SERVER : string, EXT : string, "BOOTID.UPNP.ORG" : number, USN : string, ST : string, DATE : string}, info : {address : string, family : string, port : number, size : number}) : Promise<UPNPServer>
    {
        let location = header["LOCATION"];
        let res = await fetch(location,
        {
          method : 'GET'
        });

        let xmlString = await res.text();
        let prefix = "<controlURL>";

        let controlMatch = xmlString.match(prefix + "/ContentDirectory/.*\.xml")
        let controlPostfix = ""
        if (controlMatch != null)
        {
            controlPostfix = controlMatch[0].replace(prefix, "");
        }

        let friendlyNameMatch = xmlString.match("<friendlyName>.*</friendlyName>")
        let friendlyName = ""
        if (friendlyNameMatch != null)
        {
            friendlyName = friendlyNameMatch[0].replace("<friendlyName>", "").replace("</friendlyName>", "");
        }

        return new UPNPServer(header, info, controlPostfix, friendlyName);
    }

    private constructor(header : {LOCATION : string, "CACHE-CONTROL" : string, SERVER : string, EXT : string, "BOOTID.UPNP.ORG" : number, USN : string, ST : string, DATE : string}, info : {address : string, family : string, port : number, size : number}, controlPostfix : string, friendlyName :string)
    {
        this.location = header["LOCATION"];
        this.cacheControl = header["CACHE-CONTROL"];
        this.server = header["SERVER"];
        this.ext = header["EXT"];
        this.upnpBootID = header["BOOTID.UPNP.ORG"];
        this.usn = header["USN"];
        this.st = header["ST"];
        this.date = header["DATE"];

        this.ip = info.address;
        this.family = info.family;
        this.port = info.port;
        this.size = info.size;

        let port4ControlURL = this.location.replace(new RegExp(".*" + this.ip), "").replace("/DeviceDescription.xml", "");
        this.controlUrl = "http://" + this.ip + port4ControlURL + controlPostfix;
        this.friendlyName = friendlyName;
    }

    isEqualTo(otherServer : UPNPServer) : boolean
    {
        return this.location == otherServer.location;
    }
}
