let bodyParser = require("body-parser");
let browseServer = require('dlna-browser-utils');
let config = require('config');
let Client = require('node-ssdp').Client
let express = require('express');
let EventEmitter = require('events');
let { Discover, DenonHeos } = require('denon-heos');
let discover = new Discover();


let fs = require('fs');
let logger = require('./Logger');
let morgan = require('morgan');
let split = require('split');

import { StreamServer } from './StreamServer';
import { TreeNodeDLNAContainer, TreeNodeDLNAItem } from './TreeNode';
import { UPNPServer } from './UPNPserver';

let app = express();
let distDir = "dist/soeh-web/src";
let cacheDir = "cache/";
let heosConnectionMap = new Map<number,
    [{ name: string, pid: number, model: string, version: string, ip: string, network: string, lineout: number, serial: string }, typeof DenonHeos]
>;
let streamServerMap = new Map<string, Array<StreamServer>>();
let eventEmitter = new EventEmitter();

app.use(bodyParser.json());
app.use(express.static(distDir));
app.use(morgan('combined', {
    skip: (req: any, res: any) => {
        return res.statusCode >= 400;
    }, stream: split().on('data', (data: any) => logger.info(data))
}));
app.use(morgan('combined', {
    skip: (req: any, res: any) => {
        return res.statusCode < 400;
    }, stream: split().on('data', (data: any) => logger.error(data))
}));

discover.on('device', (device: any) => {
    device.instance.connect().then(async () => {
        let ip = device.instance.address;
        let heos = new DenonHeos({ address: ip });
        heos.connect();
        heos.on('state', (state: string) => {
            logger.info('State of ' + heos.address + ': ' + state);
        });
        heos.on('connecting', () => {
            logger.info('Connecting... ' + heos.address);
        });
        heos.on('disconnect', () => {
            logger.info('Disconnected ' + heos.address);
        });
        heos.on('disconnecting', () => {
            logger.info('Disconnecting... ' + heos.address);
        });
        heos.on('reconnecting', () => {
            logger.info('Reconnecting... ' + heos.address);
        });
        heos.on('reconnected', () => {
            logger.info('Reconnected ' + heos.address);
        });

        heos.systemRegisterForChangeEvents({});
        heos.on('event', (data: any) => {
            logger.info('Event:' + JSON.stringify(data));

            let eventName = data.event;
            let message = data.message;
            switch (eventName) {
                case 'sources_changed':
                    break;
                case 'players_changed':
                    break;
                case 'group_changed':
                    break;
                case 'player_state_changed':
                    eventEmitter.emit('push', eventName, message)
                    break;
                case 'player_now_playing_changed':
                    eventEmitter.emit('push', eventName, message)
                    break;
                case 'player_now_playing_progress':
                    eventEmitter.emit('push', eventName, message)
                    break;
                case 'player_playback_error':
                    break;
                case 'player_queue_changed':
                    eventEmitter.emit('push', eventName, message)
                    break;
                case 'player_volume_changed':
                    eventEmitter.emit('push', eventName, message)
                    break;
                case 'repeat_mode_changed':
                    eventEmitter.emit('push', eventName, message)
                    break;
                case 'shuffle_mode_changed':
                    eventEmitter.emit('push', eventName, message)
                    break;
                case 'group_volume_changed':
                    break;
                case 'user_changed':
                    break;
                default:
                    logger.info('Event:' + JSON.stringify(data));
            }
        });

        let response = await heos.playerGetPlayers();
        response.forEach((player: { name: string, pid: number, model: string, version: string, ip: string, network: string, lineout: number, serial: string }) => {
            if (player.ip == heos.address)
            {
                heosConnectionMap.set(player.pid, [player, heos]);
            }
        })
    }).catch(console.error);
})
discover.start();
getServers()

app.get('/sse', function (req: any, res: any) {
    res.status(200).set({
        "connection": "keep-alive",
        "content-type": "text/event-stream"
    });

    eventEmitter.on('push', function (event: any, data: any) {
        res.write('event: ' + event + '\n' + 'data: ' + JSON.stringify(data) + '\n\n');
    });
});

app.put('/updateHeosDeviceList', function (req: any, res: any) {
    let ret: Record<number, any> = {};
    heosConnectionMap.forEach((value: [Object, typeof DenonHeos], pid: number) => {
        ret[pid] = value[0];
    });

    logger.debug("Updated heos device list" + JSON.stringify(ret));

    res.status(200).json(ret)
});

app.put('/updateStreamServerList', function (req: any, res: any) {

    let ret: Record<string, any> = {};
    streamServerMap.forEach(function (serverList, key) {
        ret[key] = serverList;
    });

    logger.debug("Updated stream server list" + JSON.stringify(ret));

    res.status(200).json(ret);
});

app.put('/updateDLNAServerList', function (req: any, res: any) {
    getServers();
    updateSIDs();

    let ret: Record<string, any> = {};
    upnpServerMap.forEach(function (tree, server) {
        if (config.get("dlna-servers").includes(server.ip))
            ret[server.ip] = server;
    });

    logger.debug("Updated dlna server list" + JSON.stringify(ret));

    res.status(200).json(ret);
});

app.put('/getDLNAServerContent', function (req: any, res: any) {
    let ip = req.body.ip;

    let ret = {};
    upnpServerMap.forEach(function (tree, server) {
        if (ip == server.ip)
            ret = tree;
    })

    logger.debug("Send content of the DLNA server " + ip);

    res.status(200).json(ret);
});

app.put('/loadHeosDevice', async function (req: any, res: any) {
    let pid = req.body.pid;

    let status = 200;
    let ret = { type: "", song: "", album: "", artist: "", image_url: "", album_id: "", mid: "", qid: "", sid: 1024, queue: [] };
    let heosConnectionTuple = heosConnectionMap.get(pid);
    if (heosConnectionTuple != undefined)
    {
        ret = await heosConnectionTuple[1].playerGetNowPlayingMedia({ pid });
        if (ret.artist == ret.song && ret.artist == "Url Stream")
        {
            logger.info(JSON.stringify(ret));
            streamServerMap.forEach((streamServerList: StreamServer[], streamType: string) => {
                for (let streamServer of streamServerList)
                {
                    if (streamServer.streamTitle == ret.album)
                    {
                        ret.image_url = streamServer.imageUrl;
                        break;
                    }
                }
            });
        }
        logger.info(JSON.stringify(ret));

        let queueResponse = await heosConnectionTuple[1].playerGetQueue({ pid });
        let queue = queueResponse.payload;
        let i = 1;
        while (queueResponse.payload.length == 100)
        {
            let range = String(i * 100) + "," + String(i * 100 + 100)
            queueResponse = await heosConnectionTuple[1].playerGetQueue({ pid, range });
            queue = queue.concat(queueResponse.payload);

            i++;
        }
        ret.queue = queue;

        logger.debug("Content of the heos device \"" + pid + "\": " + JSON.stringify(ret));
    }
    else
    {
        status = 500;
        logger.error("There is no device with the pid \"" + pid + "\" to load");
    }


    res.status(status).json(ret)
});

app.put('/addTrackToQueue', async function (req: any, res: any) {
    let pid = req.body.pid;
    let containerId = req.body.containerId;
    let songId = req.body.songId;
    let aid = req.body.aid;
    let ip = req.body.ip;

    let sid: number | undefined;
    upnpServerMap.forEach(function (tree, currentServer) {
        if (currentServer.ip == ip)
        {
            sid = currentServer.sid;
        }
    });

    let status = 200;
    let ret = {}
    let heosConnectionTuple = heosConnectionMap.get(pid);
    if (heosConnectionTuple != undefined)
    {
        ret = await heosConnectionTuple[1].browseAddTrackToQueue({ sid, cid: adjustId4Heos(containerId), mid: adjustId4Heos(songId), aid, pid });
        logger.debug("Add track " + JSON.stringify(ret));
    }
    else
    {
        status = 500;
        logger.error("There is no device with the pid \"" + pid + "\" to add the track to");
    }

    res.status(status).json(ret)
});

app.put('/addContainerToQueue', async function (req: any, res: any) {
    let pid = req.body.pid;
    let containerId = req.body.containerId;
    let aid = req.body.aid;
    let ip = req.body.ip;

    let status = 200;
    let sid: number | undefined;
    upnpServerMap.forEach(function (tree, currentServer) {
        if (currentServer.ip == ip)
        {
            sid = currentServer.sid;
        }
    });

    let ret = {}
    let heosConnectionTuple = heosConnectionMap.get(pid);
    if (heosConnectionTuple != undefined)
    {
        ret = await heosConnectionTuple[1].browseAddToQueue({ sid, cid: adjustId4Heos(containerId), aid, pid });
        logger.debug("Added container " + JSON.stringify(ret));
    }
    else
    {
        status = 500;
        logger.error("There is no device with the pid \"" + pid + "\" to add a container to");
    }

    res.status(status).json(ret)
});

app.put('/clearQueue', async function (req: any, res: any) {
    let pid = req.body.pid;

    let status = 200;
    let ret = {};
    let heosConnectionTuple = heosConnectionMap.get(pid);
    if (heosConnectionTuple != undefined)
    {
        ret = await heosConnectionTuple[1].playerClearQueue({ pid });
        logger.debug("Clear queue" + JSON.stringify(ret));
    }
    else
    {
        status = 500;
        logger.error("There is no device with the pid \"" + pid + "\" to clear the queue on");
    }

    res.status(status).json(ret)
});

app.put('/playFromQueue', async function (req: any, res: any) {
    let pid = req.body.pid;
    let qid = req.body.qid;

    let status = 200;
    let ret = {};
    let heosConnectionTuple = heosConnectionMap.get(pid);
    if (heosConnectionTuple != undefined)
    {
        ret = await heosConnectionTuple[1].playerPlayQueueItem({ pid, qid });
        logger.debug("Play from queue" + JSON.stringify(ret));
    }
    else
    {
        status = 500;
        logger.error("There is no device with the pid \"" + pid + "\" to play from the queue");
    }


    res.status(status).json(ret)
});

app.put('/moveQueueItem', async function (req: any, res: any) {
    let pid = req.body.pid;
    let sqid = req.body.source_qid;
    let dqid = req.body.destination_qid;

    let status = 200;
    let ret = {};
    let heosConnectionTuple = heosConnectionMap.get(pid);
    if (heosConnectionTuple != undefined)
    {
        ret = await heosConnectionTuple[1].playerMoveQueue({ pid, sqid, dqid });
        logger.debug("Move queue item " + JSON.stringify(ret));
    }
    else
    {
        status = 500;
        logger.error("There is no device with the pid \"" + pid + "\" to move a queue item");
    }

    res.status(status).json(ret)
});

app.put('/removeFromQueue', async function (req: any, res: any) {
    let pid = req.body.pid;
    let qid = req.body.qid;

    let status = 200;
    let ret = {};
    let heosConnectionTuple = heosConnectionMap.get(pid);
    if (heosConnectionTuple != undefined)
    {
        ret = await heosConnectionTuple[1].playerRemoveFromQueue({ pid, qid });
        logger.debug("Remove from queue " + JSON.stringify(ret));
    }
    else
    {
        status = 500;
        logger.error("There is no device with the pid \"" + pid + "\" to remove an element from the queue");
    }


    res.status(status).json(ret)
});

app.put('/getVolume', async function (req: any, res: any) {
    let pid = req.body.pid;

    let status = 200;
    let ret = {};
    let heosConnectionTuple = heosConnectionMap.get(pid);
    if (heosConnectionTuple != undefined)
    {
        ret = await heosConnectionTuple[1].playerGetVolume({ pid });
        logger.debug("Get volume " + JSON.stringify(ret));
    }
    else
    {
        status = 500;
        logger.error("There is no device with the pid \"" + pid + "\" to get the volume from");
    }

    res.status(status).json(ret)
});

app.put('/setVolume', async function (req: any, res: any) {
    let pid = req.body.pid;
    let volume = req.body.volume;

    let status = 200;
    let ret = {};
    let heosConnectionTuple = heosConnectionMap.get(pid);
    if (heosConnectionTuple != undefined)
    {
        ret = await heosConnectionTuple[1].playerSetVolume({ pid: pid, level: volume });
        logger.debug("Set volume " + JSON.stringify(ret));
    }
    else
    {
        status = 500;
        logger.error("There is no device with the pid \"" + pid + "\" to set the volume");
    }

    res.status(status).json(ret)
});

app.put('/togglePlayState', async function (req: any, res: any) {
    let pid = req.body.pid;

    let status = 200;
    let heosConnectionTuple = heosConnectionMap.get(pid);
    if (heosConnectionTuple != undefined)
    {
        let playStateResponse = await heosConnectionTuple[1].playerGetPlayState({ pid })

        let stateToSend = 'pause';
        if (playStateResponse.state == 'pause' || playStateResponse.state == 'stop')
        {
            stateToSend = 'play';
        }

        heosConnectionTuple[1].playerSetPlayState({ pid: pid, state: stateToSend })
        logger.debug("Toggle play state " + JSON.stringify(playStateResponse));
    }
    else
    {
        status = 500;
        logger.error("There is no device with the pid \"" + pid + "\" to toggle the play state on");
    }

    res.status(status);
});

app.put('/toggleRepeatMode', async function (req: any, res: any) {
    let pid = req.body.pid;

    let status = 200;
    let ret = {};
    let heosConnectionTuple = heosConnectionMap.get(pid);
    if (heosConnectionTuple != undefined)
    {
        let playStateResponse = await heosConnectionTuple[1].playerGetPlayMode({ pid });

        let shuffle = false;
        if (playStateResponse.shuffle == 'on')
        {
            shuffle = true;
        }

        let repeat = "off";
        if (playStateResponse.repeat == 'on_all')
        {
            repeat = "on_one";
        }
        else if (playStateResponse.repeat == 'on_one')
        {
            repeat = "off";
        }
        else
        {
            repeat = "on_all";
        }

        ret = await heosConnectionTuple[1].playerSetPlayMode({ pid, shuffle, repeat });
        logger.debug("Toggle repeat mode " + JSON.stringify(ret));
    }
    else
    {
        status = 500;
        logger.error("There is no device with the pid \"" + pid + "\" to toggle the repeat mode");
    }

    res.status(status).json(ret);
});

app.put('/toggleShuffleMode', async function (req: any, res: any) {
    let pid = req.body.pid;

    let status = 200;
    let ret = {};
    let heosConnectionTuple = heosConnectionMap.get(pid);
    if (heosConnectionTuple != undefined)
    {
        let playStateResponse = await heosConnectionTuple[1].playerGetPlayMode({ pid });

        let repeat = playStateResponse.repeat;
        let shuffle = false;
        if (playStateResponse.shuffle == 'off')
        {
            shuffle = true;
        }

        ret = await heosConnectionTuple[1].playerSetPlayMode({ pid, shuffle, repeat });
        logger.debug("Toggle shuffle mode " + JSON.stringify(ret));
    }
    else
    {
        status = 500;
        logger.error("There is no device with the pid \"" + pid + "\" to toggle the shuffle mode on");
    }

    res.status(status).json(ret);
});

app.put('/getPlayMode', async function (req: any, res: any) {
    let pid = req.body.pid;

    let status = 200;
    let ret = {};
    let heosConnectionTuple = heosConnectionMap.get(pid);
    if (heosConnectionTuple != undefined)
    {
        ret = await heosConnectionTuple[1].playerGetPlayMode({ pid });
        logger.debug("Get play mode " + JSON.stringify(ret));
    }
    else
    {
        status = 500;
        logger.error("There is no device with the pid \"" + pid + "\" to get the play mode");
    }

    res.status(status).json(ret);
});

app.put('/toggleMute', async function (req: any, res: any) {
    let pid = req.body.pid;

    let status = 200;
    let ret = {};
    let heosConnectionTuple = heosConnectionMap.get(pid);
    if (heosConnectionTuple != undefined)
    {
        ret = await heosConnectionTuple[1].playerToggleMute({ pid });
        logger.debug("Toggle mute " + JSON.stringify(ret));
    }
    else
    {
        status = 500;
        logger.error("There is no device with the pid \"" + pid + "\" to toggle mute");
    }

    res.status(status).json(ret);
});

app.put('/next', function (req: any, res: any) {
    let pid = req.body.pid;

    let status = 200;
    let heosConnectionTuple = heosConnectionMap.get(pid);
    if (heosConnectionTuple != undefined)
    {
        heosConnectionTuple[1].playerPlayNext({ pid: pid });
        logger.debug("Play next track on " + pid);
    }
    else
    {
        status = 500;
        logger.error("There is no device with the pid \"" + pid + "\" to play the next track");
    }

    res.status(status)
})

app.put('/previous', function (req: any, res: any) {
    let pid = req.body.pid;

    let status = 200;
    let heosConnectionTuple = heosConnectionMap.get(pid);
    if (heosConnectionTuple != undefined)
    {
        heosConnectionTuple[1].playerPlayPrevious({ pid: pid });
        logger.debug("Play previous on " + pid);
    }
    else
    {
        status = 500;
        logger.error("There is no device with the pid \"" + pid + "\" to play the previous track");
    }

    res.status(status)
})

app.put('/playStream', function (req: any, res: any) {
    let pid = req.body.pid;
    let url = req.body.url;

    let status = 200;
    let heosConnectionTuple = heosConnectionMap.get(pid);
    if (heosConnectionTuple != undefined)
    {
        heosConnectionTuple[1].browseURL({ pid: pid, url: url });
        logger.debug("Play stream " + url + " on " + pid);
    }
    else
    {
        status = 500;
        logger.error("There is no device with the pid \"" + pid + "\"to play the stream " + url);
    }

    res.status(status)
})

app.listen(config.get("app.port"), function () { });
let upnpServerMap: Map<UPNPServer, TreeNodeDLNAContainer> = new Map();

function adjustId4Heos(id: string)
{
    return id.replace(/%/g, "%25").replace(/&/g, "%26").replace(/=/g, "%3D")
}

function downloadDatabaseFromDLNAServer(idQueue: Array<TreeNodeDLNAContainer>, controlUrl: string, server: UPNPServer, counter: number, backupUsed: boolean)
{
    let parentNode = idQueue.shift();

    if (parentNode == undefined)
        return;

    let firstChild = parentNode.items?.at(0);
    if (backupUsed && firstChild != undefined && firstChild.items?.at(0) != undefined)
    {
        parentNode.items?.forEach((item) => {
            idQueue.push(item);
        })

        if (idQueue.length > 0)
        {
            downloadDatabaseFromDLNAServer(idQueue, controlUrl, server, counter, backupUsed);
        }
        else
        {
            fs.writeFile(cacheDir + server.ip, JSON.stringify(upnpServerMap.get(server)), (error: any) => {
                if (error)
                {
                    logger.info("Couldn't save to file for " + counter);
                    logger.error(JSON.stringify(error));
                }
            });
        }
    }
    else
    {
        if (counter % 1000 == 0)
        {
            fs.writeFile(cacheDir + server.ip, JSON.stringify(upnpServerMap.get(server)), (error: any) => {
                if (error)
                {
                    logger.info("Couldn't save to file for " + counter);
                    logger.error(JSON.stringify(error));
                }
            });
        }

        let index = parentNode.items?.length;
        browseServer(parentNode.id, controlUrl, { startIndex: index }, function (err: any, result: any) {
            if (err)
            {
                logger.error(JSON.stringify(err));
                return;
            }

            if (parentNode != null)
            {
                if (result.container)
                {
                    for (let i = 0; i < result.container.length; i++)
                    {
                        let container = new TreeNodeDLNAContainer(result.container[i], false);
                        parentNode.items?.push(container);
                        idQueue.push(container);
                    }
                }

                if (result.item)
                {
                    for (let i = 0; i < result.item.length; i++)
                    {
                        parentNode.items?.push(new TreeNodeDLNAItem(result.item[i]));
                    }
                }

                if (result.container != undefined || result.item != undefined)
                {
                    idQueue.unshift(parentNode);
                }
                else if (backupUsed)
                {
                    parentNode.items?.forEach((item: TreeNodeDLNAContainer) => {
                        idQueue.push(item);
                    });
                }
            }

            if (idQueue.length > 0)
            {
                downloadDatabaseFromDLNAServer(idQueue, controlUrl, server, ++counter, backupUsed);
            }
            else
            {
                fs.writeFile(cacheDir + server.ip, JSON.stringify(upnpServerMap.get(server)), (error: any) => {
                    if (error)
                    {
                        logger.info("Couldn't save to file for " + counter);
                        logger.error(JSON.stringify(error));
                    }
                });
            }
        });
    }
}

function getServers()
{
    let streamTypesList = config.get("streams");
    for (let i = 0; i < streamTypesList.length; i++)
    {
        let streamServerList = new Array<StreamServer>();

        let streamMap = streamTypesList[i];
        let streamType = Object.keys(streamMap)[0];
        let streamList = streamMap[streamType];
        for (let j = 0; j < streamList.length; j++)
        {
            streamServerList.push(new StreamServer(streamList[j]));
        }

        streamServerMap.set(streamType, streamServerList);
    };


    let client = new Client();

    client.on('response', function (headers: { LOCATION: string, "CACHE-CONTROL": string, SERVER: string, EXT: string, "BOOTID.UPNP.ORG": number, USN: string, ST: string, DATE: string }, statusCode: number, rinfo: { address: string, family: string, port: number, size: number }) {
        UPNPServer.createNewUPNPServer(headers, rinfo).then((newServer => {
            let alreadyFound = false;

            upnpServerMap.forEach(function (tree, server) {
                if (newServer.isEqualTo(server))
                {
                    alreadyFound = true;
                }
            });

            if (!alreadyFound && config.get("dlna-servers").includes(newServer.ip))
            {
                let treeDatabase = new TreeNodeDLNAContainer({ $: { id: 'musicdb://artists/', searchable: 0 } }, true);
                let backupUsed = false;
                try
                {
                    let data = fs.readFileSync(cacheDir + newServer.ip, 'utf8');
                    treeDatabase = JSON.parse(data);
                    backupUsed = true;
                }
                catch (err)
                {
                    logger.error(JSON.stringify(err));
                }

                upnpServerMap.set(newServer, treeDatabase);
                var queue = new Array();
                queue.push(treeDatabase);
                downloadDatabaseFromDLNAServer(queue, newServer.controlUrl, newServer, 0, backupUsed);
            }
        }));
    });
    client.search('urn:schemas-upnp-org:device:MediaServer:1');
}

async function updateSIDs() {
    let heosConnectionTuple = heosConnectionMap.values().next().value; // get any heos connection
    if (heosConnectionTuple != undefined)
    {
        let sidForLocalMusic = 1024;
        let sourceInfoList = await heosConnectionTuple[1].browseBrowseSource({ sid: sidForLocalMusic });

        sourceInfoList.forEach((sourceInfo: { name: string, image_uri: string, image_url: string, type: string, sid: number }) => {
            upnpServerMap.forEach((tree, server) => {
                if (server.friendlyName == sourceInfo.name)
                {
                    server.sid = sourceInfo.sid;
                }
            })
        });

    }
}