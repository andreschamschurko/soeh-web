import { Component, enableProdMode, ViewChild, OnInit } from '@angular/core';
import { StreamServer } from '../../backend/StreamServer';
import { TreeNodeDLNAContainer } from '../../backend/TreeNode';
import { DxDrawerComponent } from 'devextreme-angular/ui/drawer';
import DataSource from 'devextreme/data/data_source';

if (!/localhost/.test(document.location.host)) {
  enableProdMode();
}

@Component({
selector: 'app-root',
templateUrl: './app.component.html',
styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit
{
  title = 'soeh-web'
  @ViewChild(DxDrawerComponent) drawer!: DxDrawerComponent;
  eventSource!: EventSource;


  defaultImageSource = "assets/folder.jpg";
  defaultLabelText = "unknown - unknown";

  imageSource = this.defaultImageSource;
  labelText = this.defaultLabelText;

  muteIcon = "isnotblank";
  repeatIcon = "arrowright";
  shuffleIcon = "sortdown";

  inProgress = false;
  isDLNAPopupVisible = false;
  isStreamPopupVisible = false;

  maxValue = 220;
  seconds = 0;
  intervalId = 1;

  isDrawerOpen = false;

  navigation : any;

  dlnaGridData = new Array<TreeNodeDLNAContainer>();
  dlnaGridDataSource = new DataSource({
    store: {  
      type: 'array',  
      key:  'id',  
      data: this.dlnaGridData
    }
  });

  streamGridData = new Array<StreamServer>();
  streamGridDataSource = new DataSource({
    store: {  
      type: 'array',  
      key:  'id',  
      data: this.streamGridData
    }
  });

  queueData : any;
  volumeLevel = 50;

  deviceList = new Array<string>();
  dlnaServerList = new Array<string>();
  dlnaServerMap = new Map<string, string>();
  streamServerMap = new Map<string, Array<StreamServer>>();
  currentPID = "";
  currentHeosName = "";
  currentDLNAIP = "";
  currentDLNAName = "";

  currentStreamType = "";
  
  dlnaToolbarOptions = {
    dataSource: this.dlnaServerList,
    value: this.currentDLNAName,
    onValueChanged : (event : any) => {
      this.currentDLNAName = event.value;
      this.currentDLNAIP = this.dlnaServerMap.get(this.currentDLNAName) || "";
      this.getDLNAServerContent();
    }
  };
  
  streamToolbarOptions = {
    dataSource: Array.from(this.streamServerMap.keys()),
    value: this.currentStreamType,
    onValueChanged : (event : any) => {
      this.currentStreamType = event.value;
      this.streamGridData = this.streamServerMap.get(this.currentStreamType)!;
    }
  };

  constructor()
  {
    this.removeFromQueue = this.removeFromQueue.bind(this);
  }
  
  public ngOnInit(): void
  {
    this.eventSource = new EventSource("sse");
    this.eventSource.addEventListener('player_now_playing_changed', message => {
      let data = JSON.parse(message.data);

      if (data.pid == this.currentPID)
      {
        this.loadDevice();
      }
    });
    this.eventSource.addEventListener('player_now_playing_progress', message => {
      let data = JSON.parse(message.data);

      if (data.pid == this.currentPID)
      {
        clearInterval(this.intervalId);
        this.seconds = Number(data.cur_pos) / 1000;
        this.maxValue = Number(data.duration) / 1000;

        if (this.maxValue == 0 && this.seconds > this.maxValue)
        {
          this.maxValue = this.seconds + 5;
        }

        this.intervalId = window.setInterval(() => this.timer(), 1000);
        this.inProgress = true;
      }
    });
    this.eventSource.addEventListener('player_state_changed', message => {
      let data = JSON.parse(message.data);

      if (data.pid == this.currentPID)
      {
        if (data.state == "pause" || data.state == "stop")
        {
          this.inProgress = false;
        }
        else
        {
          clearInterval(this.intervalId);
          this.intervalId = window.setInterval(() => this.timer(), 1000);
          this.inProgress = true;
        }
      }
    });
    this.eventSource.addEventListener('player_queue_changed', message => {
      let data = JSON.parse(message.data);

      if (data.pid == this.currentPID)
      {
        this.loadDevice();
      }
    });
    this.eventSource.addEventListener('player_volume_changed', message => {
      let data = JSON.parse(message.data);

      if (data.pid == this.currentPID)
      {
        this.getVolume();

        if (data.mute == "on")
          this.muteIcon = "clear";
        else
          this.muteIcon = "isnotblank";
      }
    });
    this.eventSource.addEventListener('repeat_mode_changed', message => {
      let data = JSON.parse(message.data);

      if (data.pid == this.currentPID)
        this.updateRepeatMode(data.repeat);
    });
    this.eventSource.addEventListener('shuffle_mode_changed', message => {
      let data = JSON.parse(message.data);

      if (data.pid == this.currentPID)
        this.updateShuffleMode(data.shuffle);
    });
  }

  onDragEnd(event : any)
  {
    if (event.fromIndex === event.toIndex)
      return;

    let data = { pid : event.element.id, source_qid : event.itemData.qid , destination_qid : event.toIndex + 1 };
    fetch("moveQueueItem",
    {
      method : 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  }

  onDragStart(event : any)
  {
    event.itemData = event.fromData[event.fromIndex];
  }

  async toggleDLNAPopup()
  {
    this.isDLNAPopupVisible = (this.isDLNAPopupVisible ? false : true);
    
    if (this.isDLNAPopupVisible)
    {
      let res = await fetch("updateDLNAServerList",
      {
        method : 'PUT'
      });

      res.json().then(data => {
        this.dlnaServerList = new Array<string>();

        Object.keys(data).forEach((ip : string) => {
          this.dlnaServerList.push(data[ip].friendlyName);
          this.dlnaServerMap.set(data[ip].friendlyName, ip);
        });

        this.currentDLNAName = this.dlnaServerList[0];
        if (this.currentDLNAIP != this.dlnaServerMap.get(this.currentDLNAName))
        {
          this.currentDLNAIP = this.dlnaServerMap.get(this.currentDLNAName) || "";
          this.getDLNAServerContent();
        }

        this.dlnaToolbarOptions = {
          dataSource: this.dlnaServerList,
          value: this.currentDLNAName,
          onValueChanged : (event : any) => {
            this.currentDLNAName = event.value;
            this.currentDLNAIP = this.dlnaServerMap.get(this.currentDLNAName) || "";
            this.getDLNAServerContent();
          }
        }
      });
    }
  }

  async getDLNAServerContent()
  {
    let requestData = { ip : this.currentDLNAIP };
    let res = await fetch("getDLNAServerContent",
    {
      method : 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(requestData),
    });

    let songs = new Array<TreeNodeDLNAContainer>();

    res.json().then(data => {
      data.items?.forEach((artist : TreeNodeDLNAContainer) => {
        artist.items?.forEach((album : TreeNodeDLNAContainer) => {
          album.items?.forEach((song : TreeNodeDLNAContainer) => songs.push(song))
        });
      });
    });

    this.dlnaGridData = songs;
  }

  async toggleStreamPopup()
  {
    this.isStreamPopupVisible = (this.isStreamPopupVisible ? false : true);
    this.updateStreamServerList();
  }

  async updateStreamServerList()
  {
    let res = await fetch("updateStreamServerList",
    {
      method : 'PUT',
      headers: {
        'Content-Type': 'application/json',
      }
    });

    res.json().then(data => {
      let keys = Object.keys(data);
      for (let i = 0; i < keys.length; i++)
      {
        this.streamServerMap.set(keys[i], data[keys[i]]);
      }

      if (!this.streamServerMap.has(this.currentStreamType) && this.streamServerMap.size > 0)
      {
        this.currentStreamType = this.streamServerMap.keys().next().value;
        this.streamGridData = this.streamServerMap.get(this.currentStreamType)!;
      }

      this.streamToolbarOptions = {
        dataSource: Array.from(this.streamServerMap.keys()),
        value: this.currentStreamType,
        onValueChanged : (event : any) => {
          this.currentStreamType = event.value;
          this.streamGridData = this.streamServerMap.get(this.currentStreamType)!;
        }
      }
    });
  }

  onContentReady(event : any)
  {
    event.component.option('loadPanel.enabled', false);
  }

  async addMenuItems(event : any)
  { 
    if (event.target == 'content')
    {
      if (!event.items)
        event.items = [];

      event.items.push({
          text: 'Play now',
          onItemClick: () => this.addToQueue(event.row, 1)
      });
      event.items.push({
          text: 'Play next',
          onItemClick: () => this.addToQueue(event.row, 2)
      });
      event.items.push({
          text: 'Add to the end of the queue',
          onItemClick: () => this.addToQueue(event.row, 3)
      });
      event.items.push({
          text: 'Replace and play',
          onItemClick: () => this.addToQueue(event.row, 4)
      });
    } 
  }
  
  async addStreamMenuItems(event : any)
  {
    if (event.target == 'content')
    {
      if (!event.items)
        event.items = [];

      event.items.push({
          text: 'Play now',
          onItemClick: () => this.playStream(event.row.data.url)
      });
    }
  }

  addToQueue(row : any, aid : number)
  {
    let data = {
      pid: this.currentPID, 
      containerId : row.data.parentID, 
      songId : row.data.id,
      aid : aid,
      ip : this.currentDLNAIP
    }

    if (row.data.items == undefined || row.data.items.length == 0)
    {
      fetch("addTrackToQueue",
      {
        method : 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      });
    }
    else
    {
      if (Array.isArray(row.data.key)) // Check whether the container is an album
      {
        data.containerId = row.data.items[0].parentID; 
      }
      else
      {
        // remove the prefix musicdb://artists/ and from the result everything from the first slash to the end
        let artistId = row.data.items[0].items[0].parentID.replace("musicdb://artists/", "").replace(/\/.*$/, "");

        // construct containerId using the artistId
        data.containerId = "musicdb://artists/" + artistId + "/?albumartistsonly=true";
      }
        
      fetch("addContainerToQueue",
      {
        method : 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      });
    }
  }

  toggleSidebar()
  {
    this.isDrawerOpen = (this.isDrawerOpen ? false : true);

    this.updateHeosDeviceList();
  }

  playPauseToggleButtonClick()
  {
    this.togglePlayState()

    if (this.inProgress)
    {
      clearInterval(this.intervalId);
    }
    else
    {
      this.intervalId = window.setInterval(() => this.timer(), 1000);
    }
    this.inProgress = !this.inProgress;
  }

  timer() 
  {
    this.seconds++;
    if (this.seconds == this.maxValue)
    {
      this.inProgress = !this.inProgress;
      clearInterval(this.intervalId);
    }
  }

  statusFormat(ratio : number, value : number)
  {
    return timeFormat(value) + "/" + timeFormat(value / ratio);
  }

  loadDeviceFromEvent(event : any)
  {
    this.currentPID = this.deviceList[event.addedItems[0].id];
    this.currentHeosName = event.addedItems[0].text;
    
    this.loadDevice();
    this.getVolume();
    this.getPlayMode();
  }

  async loadDevice()
  {
    let requestData = { pid : this.currentPID }
    let res = await fetch("loadHeosDevice",
    {
      method : 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(requestData),
    });

    res.json().then(data => {
      this.queueData = data.queue;

      if (Object.keys(data).length > 0)
      {
        this.imageSource = data.image_url;
        if (data.artist == data.song && data.artist == "Url Stream")
        {
          this.labelText = data.album;
        }
        else
        {
          this.labelText = data.artist + " / " + data.album + " / " + data.song;
        }

        if (this.queueData.length > 0)
        {
          // Set the current item in the queue if the current played song is not from TuneIn
          // because TuneIn streams are not added to the queue. The heos devices still send
          // the qid 1 which needs to be ignored
          if (data.sid != 3)
          {
            this.queueData[data.qid - 1].isCurrent = true;
          }
          else if (data.artist == data.song && data.artist == "Url Stream")
          {
            // Set the album and image_url of the current song in the queue if the current
            // song is a url stream, as the heos device does not provide any information for
            // url streams in the queue
            this.queueData[data.qid - 1].album = data.album;
            this.queueData[data.qid - 1].image_url = data.image_url;
          }
        }
      }
      else
      {
        this.imageSource = this.defaultImageSource;
        this.labelText = this.defaultLabelText;
      }

    })
  }

  async updateHeosDeviceList()
  {
    let res = await fetch("updateHeosDeviceList",
      {
        method : 'PUT'
      });
    
    res.json().then((data) => {
      let items = new Array<{ id : number; text : string; icon : string; }>();

      let keys = Object.keys(data);
      this.deviceList = new Array<string>();
      for (let i = 0; i < keys.length; i++)
      {
        this.deviceList.push(data[keys[i]].pid);
        items.push({ id : i, text : data[keys[i]].name, icon : "music"});
      }

      this.navigation = items;
      this.drawer.instance.repaint();
    });
  }

  async getPlayMode()
  {
    let data = { pid : this.currentPID };
    let res = await fetch("getPlayMode",
    {
      method : 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });

    res.json().then((data) => {
      this.updateRepeatMode(data.repeat);
      this.updateShuffleMode(data.shuffle);
    });
  }

  updateRepeatMode(repeat : string)
  {
    switch (repeat)
    {
      case "off":
        this.repeatIcon = "arrowright";
        break;
      case "on_all":
        this.repeatIcon = "repeat";
        break;
      case "on_one":
        this.repeatIcon = "refresh";
        break;
    }
  }

  updateShuffleMode(shuffle : string)
  {
    if (shuffle == "on")
      this.shuffleIcon = "sorted";
    else
      this.shuffleIcon = "sortdown";
  }

  async getVolume()
  {
    let requestData = { pid : this.currentPID };
    let res = await fetch("getVolume",
      {
        method : 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(requestData),
      });
    
    res.json().then((data) => this.volumeLevel = data.level);
  }

  async updateVolume(event : any)
  {
    let requestData = { pid : this.currentPID, volume : event.srcElement.value };
    let res = await fetch("setVolume",
      {
        method : 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(requestData)
      });
    
    res.json().then((data) => this.volumeLevel = data.level);
  }

  updateServerList()
  {
    fetch("updateServerList",
      {
        method : 'PUT'
      });
  }

  togglePlayState()
  {
    let requestData = { pid : this.currentPID };
    fetch("togglePlayState",
    {
      method : 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(requestData)
    });
  }

  playNext()
  {
    let requestData = { pid : this.currentPID };
    fetch("next",
    {
      method : 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(requestData)
    });
  }

  playPrevious()
  {
    let requestData = { pid : this.currentPID };
    fetch("previous",
    {
      method : 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(requestData)
    });
  }

  toggleMute()
  {
    let data = { pid : this.currentPID };
    fetch("toggleMute",
    {
      method : 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  }

  toggleRepeatMode()
  {
    let data = { pid : this.currentPID };
    fetch("toggleRepeatMode",
    {
      method : 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  }

  toggleShuffle()
  {
    let data = { pid : this.currentPID };
    fetch("toggleShuffleMode",
    {
      method : 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  }

  clearQueue()
  {
    let data = { pid : this.currentPID };
    fetch("clearQueue",
    {
      method : 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  }

  playFromQueue(event : any)
  {
    let data = { pid : this.currentPID, qid : event.itemData.qid };
    fetch("playFromQueue",
    {
      method : 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  }

  removeFromQueue(event : any)
  {
    let data = { pid : this.currentPID, qid : event.itemData.qid };
    fetch("removeFromQueue",
    {
      method : 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
  }

  playStream(url : string)
  {
    let requestData = { pid : this.currentPID, url };
    fetch("playStream",
    {
      method : 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(requestData)
    });
  }
}

export function timeFormat(time: number)
{
  return Math.floor(time/60) + ":" + (Math.round(time%60) < 10 ? "0" : "" ) + Math.round(time%60)
}