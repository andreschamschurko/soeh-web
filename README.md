# SOEH-WEB
A web application to control devices using the heos protocol.

## Features
* Control several heos devices and switch between those
* Play music from configurable DLNA servers
    - Search music
    - Store the content of DLNA servers for reuse on application restart
    - Switch between different DLNA servers
* Play configurable radio streams

## Dependencies
* angular
* express
* [dlna-browser-utils](www.github.com/andreschamschurko/dlna-browser-utils) custom fork of www.github.com/mhdawson/dlna-browser-utils
* heos-api
* karma
* morgan
* [node-denon-heos](www.github.com/andreschamschurko/node-denon-heos) custom fork of www.github.com/WeeJeWel/node-denon-heos
* node-fetch
* node-ssdp
* rxjs
* tslib
* typescript
* winston
* winston-daily-rotate-file
* zone.js


## Building and starting
1. Building the project: `npm run build`
2. Starting the project: `npm run start`
